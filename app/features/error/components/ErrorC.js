import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '@atlaskit/button';
type Props = {

    /**
     * Redux dispatch.
     */
    dispatch: Dispatch<*>;
}
class ErrorC extends Component{
    constructor(props: Props) {
        super(props);
    }
    
    _onJoin: (*) => void;

    async _onJoin() {
        this.props.dispatch(push('/'));
    }
    render() {
        return (
            <div><h1>Error 404</h1>
            <Button
                                appearance = 'primary'
                                onClick = { this._onJoin }
                                type = 'button'
                              >
                                Back
                            </Button>
            </div>

        );
    }

}

export default connect()(ErrorC);