// @flow

import Button from '@atlaskit/button';
import { FieldTextStateless } from '@atlaskit/field-text';
import { SpotlightTarget } from '@atlaskit/onboarding';
import Page from '@atlaskit/page';
import { AtlasKitThemeProvider } from '@atlaskit/theme';

import { generateRoomWithoutSeparator } from '@jitsi/js-utils/random';
import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { compose } from 'redux';
import type { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { Navbar } from '../../navbar';
import { Onboarding, startOnboarding } from '../../onboarding';
import { RecentList } from '../../recent-list';
import { createConferenceObjectFromURL } from '../../utils';

import { Body, FieldWrapper, Form, Header, Label, Wrapper } from '../styled';

type Props = {

    /**
     * Redux dispatch.
     */
    dispatch: Dispatch<*>;

    /**
     * React Router location object.
     */
    location: Object;

    /**
     * I18next translate function.
     */
     t: Function;
};

type State = {

    /**
     * Timer for animating the room name geneeration.
     */
    animateTimeoutId: ?TimeoutID,

    /**
     * Generated room name.
     */
    generatedRoomname: string,

    /**
     * Current room name placeholder.
     */
    roomPlaceholder: string,

    /**
     * Timer for re-generating a new room name.
     */
    updateTimeoutId: ?TimeoutID,

    /**
     * URL of the room to join.
     * If this is not a url it will be treated as room name for default domain.
     */
    url: string;

    displayName: string;

    meetingId: string;
};

/**
 * Welcome Component.
 */
class Welcome extends Component<Props, State> {
    /**
     * Initializes a new {@code Welcome} instance.
     *
     * @inheritdoc
     */
    constructor(props: Props) {
        super(props);

        // Initialize url value in state if passed using location state object.
        let url = '';

        // Check and parse url if exists in location state.
        if (props.location.state) {
            const { room, serverURL } = props.location.state;

            if (room && serverURL) {
                url = `${serverURL}/${room}`;
            }
        }

        this.state = {
            animateTimeoutId: undefined,
            generatedRoomname: '',
            roomPlaceholder: '',
            updateTimeoutId: undefined,
            url,
            meetingId: '',
            displayName: ''
        };

        // Bind event handlers.
        this._animateRoomnameChanging = this._animateRoomnameChanging.bind(this);
        this._onURLChange = this._onURLChange.bind(this);
        this._onDisplayNameChange = this._onDisplayNameChange.bind(this);
        this._onMeetingIdChange = this._onMeetingIdChange.bind(this);
        this._onFormSubmit = this._onFormSubmit.bind(this);
        this._onJoin = this._onJoin.bind(this);
        this._updateRoomname = this._updateRoomname.bind(this);
    }

    /**
     * Start Onboarding once component is mounted.
     * Start generating randdom room names.
     *
     * NOTE: It autonatically checks if the onboarding is shown or not.
     *
     * @returns {void}
     */
    componentDidMount() {
        this.props.dispatch(startOnboarding('welcome-page'));

        this._updateRoomname();
    }

    /**
     * Stop all timers when unmounting.
     *
     * @returns {voidd}
     */
    componentWillUnmount() {
        this._clearTimeouts();
    }

    /**
     * Render function of component.
     *
     * @returns {ReactElement}
     */
    render() {
        return (
            <Page navigation = { <Navbar /> }>
                <AtlasKitThemeProvider mode = 'light'>
                    <Wrapper>
                        { this._renderHeader() }
                        { this._renderBody() }
                        <Onboarding section = 'welcome-page' />
                    </Wrapper>
                </AtlasKitThemeProvider>
            </Page>
        );
    }

    _animateRoomnameChanging: (string) => void;

    /**
     * Animates the changing of the room name.
     *
     * @param {string} word - The part of room name that should be added to
     * placeholder.
     * @private
     * @returns {void}
     */
    _animateRoomnameChanging(word: string) {
        let animateTimeoutId;
        const roomPlaceholder = this.state.roomPlaceholder + word.substr(0, 1);

        if (word.length > 1) {
            animateTimeoutId
                = setTimeout(
                    () => {
                        this._animateRoomnameChanging(
                            word.substring(1, word.length));
                    },
                    70);
        }
        this.setState({
            animateTimeoutId,
            roomPlaceholder
        });
    }

    /**
     * Method that clears timeouts for animations and updates of room name.
     *
     * @private
     * @returns {void}
     */
    _clearTimeouts() {
        clearTimeout(this.state.animateTimeoutId);
        clearTimeout(this.state.updateTimeoutId);
    }

    _onFormSubmit: (*) => void;

    /**
     * Prevents submission of the form and delegates the join logic.
     *
     * @param {Event} event - Event by which this function is called.
     * @returns {void}
     */
    _onFormSubmit(event: Event) {
        event.preventDefault();
        this._onJoin();
    }

    async LoginApi(_displayName: string, _meetingId) {
        /*   
           //const lastIndexOfSlash = inputURL.lastIndexOf('/');
           let room;
           let serverURL;
           */
       /*
           if (lastIndexOfSlash === -1) {
               // This must be only the room name.
               room = inputURL;
           } else {
               // Take the substring after last slash to be the room name.
               room = inputURL.substring(lastIndexOfSlash + 1);
       
               // Take the substring before last slash to be the Server URL.
               serverURL = inputURL.substring(0, lastIndexOfSlash);
       
               // Normalize the server URL.
               serverURL = normalizeServerURL(serverURL);
           }
       */
       //let _meetingId = 33824646
       
       //let k =  await fetch('https://test.roundesk.io/api/login',{
           let k =  await fetch('https://test.roundesk.io/api/login',{   
           method: 'POST', // *GET, POST, PUT, DELETE, etc.
           mode: 'cors', // no-cors, *cors, same-origin
           cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
           credentials: 'same-origin', // include, *same-origin, omit
           headers: {
             'Content-Type': 'application/json'
             // 'Content-Type': 'application/x-www-form-urlencoded',
           },
           redirect: 'follow', // manual, *follow, error
           referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
           body: JSON.stringify({//email: _email, password: _password,
               displayName: _displayName, meetingId: _meetingId}) // body data type must match "Content-Type" header
         })
       .then(res => 
           //{
           //console.log(res.json)
           res.json())
           //}
       .then(data => {
           console.log('........' + data 
           + data.user + //data.user.meetingRoomNo + data.settings + 
           data.meeting.roomId + " " +  "meetingId:" + data.meeting.meetingId + " " + "userType:" + data.meeting.userType
          )
           let div = document.createElement("div");
           div.setAttribute("id", "conditions");
           //let img = document.createElement("img");
           //img.setAttribute("src", data.current.weather_icons[0]);
           //img.setAttribute("loading", "lazy");
       
           //let weatherDesc = document.createElement("h4");
           //let weatherDescNode = document.createTextNode(data.current.weather_descriptions[0]);
           //weatherDesc.appendChild(weatherDescNode);
       
           //let city = document.createElement("h2");
           //let cityNode = document.createTextNode(data.request.query);
           //city.appendChild(cityNode);
       
           //let temp = document.createElement("div");
           //let tempNode = document.createTextNode("Your token is this......" + data.user.token );
           //temp.appendChild(tempNode);
       
           //div.appendChild(img);
           //div.appendChild(weatherDesc);
           //div.appendChild(city);
           //div.appendChild(temp);
           //document.querySelector("replyk").appendChild(div);
           // ipc.send('aSynMessageSuccess','A async message to show success')
           /*
           */
           if(data.meeting.roomId) {
               //return data.meeting.roomId
               return data
               //return "Success";
           } else {
               throw new Error('Server response wasn\'t OK');
           }
           /*
           */
           //return "success"
           
          
       }).catch(err => console.log(err))
       console.log("kkkkk", k)
       //let l = ipc.send('synMessageSuccess','A async message to show success');
       //let f = {meetingRoomNo: k, meetingId: _meetingId, success: true}
       //let l = ipc.send('synMessageSuccess',f);
       return k
       /*
           // Don't navigate if no room was specified.
           if (!room) {
               return;
           }
       
           return {
               room,
               serverURL
           };
           */
       }

    _onJoin: (*) => void;

    /**
     * Redirect and join conference.
     *
     * @returns {void}
     */
    async _onJoin() {
        //const inputURL = this.state.url || this.state.generatedRoomname;
        //const conference = createConferenceObjectFromURL(inputURL);

        // Don't navigate if conference couldn't be created
        //if (!conference) {
        //    return;
        //}
        const displayName = this.state.displayName
        console.log('displayName isss:' + " " + displayName)
        const meetingId = this.state.meetingId
        console.log('meetingId isss:' + " " + meetingId)
    
        const preCheckAnonymousUserRoomId = await this.LoginApi(displayName, meetingId);
        console.log('preCheckAnonymousUser preCheckAnonymousUserRoomId: ' + preCheckAnonymousUserRoomId);
        if(preCheckAnonymousUserRoomId !== undefined){
            console.log('Success')
            this.props.dispatch(push('/conference', preCheckAnonymousUserRoomId));
            
        } else {
            console.log('Error')
            this.props.dispatch(push('/error'));
        }

       
    }

    _onURLChange: (*) => void;
    _onDisplayNameChange: (*) => void;
    _onMeetingIdChange: (*) => void;
    /**
     * Keeps URL input value and URL in state in sync.
     *
     * @param {SyntheticInputEvent<HTMLInputElement>} event - Event by which
     * this function is called.
     * @returns {void}
     */
    _onURLChange(event: SyntheticInputEvent<HTMLInputElement>) {
        this.setState({
            url: event.currentTarget.value
        });
    }

    _onDisplayNameChange(event: SyntheticInputEvent<HTMLInputElement>) {
        this.setState({
            displayName: event.currentTarget.value
        });
    }

    _onMeetingIdChange(event: SyntheticInputEvent<HTMLInputElement>) {
        this.setState({
            meetingId: event.currentTarget.value
        });
    }

    /**
     * Renders the body for the welcome page.
     *
     * @returns {ReactElement}
     * <Body>
                <RecentList />
            </Body>
     */
    _renderBody() {
        return (
            
             <Body>

             </Body>
        );
    }

    /**
     * Renders the header for the welcome page.
     *
     * @returns {ReactElement}
     */
    _renderHeader() {
        const locationState = this.props.location.state;
        const locationError = locationState && locationState.error;
        const { t } = this.props;

        return (
            <Header>
                <SpotlightTarget name = 'conference-url'>
                <Form onSubmit = { this._onFormSubmit }>
                        <Label>{ t('enterConferenceNameOrUrl') } </Label>
                        <FieldWrapper>
                            <Label>MeetingId</Label>
                            <FieldTextStateless
                                autoFocus = { true }
                                isInvalid = { locationError }
                                isLabelHidden = { true }
                                onChange = { this._onMeetingIdChange }
                                placeholder = { this.state.meetingId }
                                shouldFitContainer = { true }
                                type = 'text'
                                value = { this.state.meetingId } />
                        <Label>Display Name</Label>
                        <FieldTextStateless
                                autoFocus = { true }
                                isInvalid = { locationError }
                                isLabelHidden = { true }
                                onChange = { this._onDisplayNameChange }
                                placeholder = { this.state.displayName }
                                shouldFitContainer = { true }
                                type = 'text'
                                value = { this.state.displayName } />
                            <Button
                                appearance = 'primary'
                                onClick = { this._onJoin }
                                type = 'button'
                                isDisabled= {this.state.displayName.length < 1 || this.state.meetingId.length < 1}>
                                { t('go') }
                            </Button>
                        </FieldWrapper>
                    </Form>

                
                </SpotlightTarget>
            </Header>
        );
    }

    _updateRoomname: () => void;

    /**
     * Triggers the generation of a new room name and initiates an animation of
     * its changing.
     *
     * @protected
     * @returns {void}
     */
    _updateRoomname() {
        const generatedRoomname = generateRoomWithoutSeparator();
        const roomPlaceholder = '';
        const updateTimeoutId = setTimeout(this._updateRoomname, 10000);

        this._clearTimeouts();
        this.setState(
            {
                generatedRoomname,
                roomPlaceholder,
                updateTimeoutId
            },
            () => this._animateRoomnameChanging(generatedRoomname));
    }
}

export default compose(connect(), withTranslation())(Welcome);

/*
Line 268
<Form onSubmit = { this._onFormSubmit }>
                        <Label>{ t('enterConferenceNameOrUrl') } </Label>
                        <FieldWrapper>
                            <FieldTextStateless
                                autoFocus = { true }
                                isInvalid = { locationError }
                                isLabelHidden = { true }
                                onChange = { this._onURLChange }
                                placeholder = { this.state.roomPlaceholder }
                                shouldFitContainer = { true }
                                type = 'text'
                                value = { this.state.url } />
                            <Button
                                appearance = 'primary'
                                onClick = { this._onJoin }
                                type = 'button'>
                                { t('go') }
                            </Button>
                        </FieldWrapper>
                    </Form>

Line 285
*/

/*
  <div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label>  
                                        Enter meetingId:  https://uat.roundesk.io/22651633#config.disableDeepLinking=true   
                                    <input  placeholder="22651633" required="" type="text"  onChange = { event => this.setState({ meetingId: event.target.value }) } value={ this.state.meetingId } />
                                    </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label>  
                                    Enter Display name: 
                                <input  placeholder="Enter Your Name" required="" type="text"  onChange = { event => this.setState({ displayName: event.target.value }) } value={ this.state.displayName} />
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input type="submit" value="Back"/>
                                <input class="fxt-btn-fill" type="submit"  disabled = {this.state.displayName.length < 1 || this.state.meetingId.length < 1} onClick = { this._onJoin }/>
                            </div>
                        </div>
                    </div>

                    */